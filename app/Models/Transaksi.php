<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian';

    protected $primaryKey = 'id';

    protected $fillable = [
        'total_harga'
    ];
}
