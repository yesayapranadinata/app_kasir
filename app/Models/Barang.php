<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'master_barang';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nama_barang','harga_satuan'
    ];
}
