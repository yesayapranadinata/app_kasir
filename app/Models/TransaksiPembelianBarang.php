<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;
    protected $table = 'master_barang';

    protected $primaryKey = 'id';

    protected $fillable = [
        'transaksi_pembelian_id','master_barang_id', 'jumlah', 'harga_satuan'
    ];
}
