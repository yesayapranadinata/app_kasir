<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Barang;

class BarangController extends Controller
{
    public function index(){
        $dataBarang = Barang::all();
        return View::first(['transaksi.barang'],['dataBarang' => $dataBarang]);
    }
}
