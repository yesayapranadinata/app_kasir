<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Transaksi;
use App\Models\TransaksiPembelianBarang;
use App\Models\Barang;
use Session;
use DB;

class TransaksiController extends Controller
{
    public function index(){
        $dataTran = Transaksi::select('id','created_at','total_harga')->get();
        return View::first(['transaksi.index'],['dataTran' =>$dataTran]);
    }
    public function tambahTransaksi(){
        $dataBarang = Barang::select('id','nama_barang','harga_satuan')->get();
        return View::first(['transaksi.tambahTransaksi'],['dataBarang' => $dataBarang]);
    }
    public function cekSubtotal(Request $request){
        
        $request->session()->reflash();
        
        $total = [];
        $barang = [];
        $jumlah = [];
        
        for($i = 0; $i < count($request->barang); $i++){
            $barangPerUnit = json_decode($request->barang[$i]);
            $jumlahPerUnit = intval($request->jumlah[$i]);

            $totalPerkalian = $barangPerUnit->harga_satuan * intval($request->jumlah[$i]);
            

            array_push($total, $totalPerkalian);
            array_push($barang, $barangPerUnit);
            array_push($jumlah, $jumlahPerUnit);


        }
        $request->session()->put([
                'barang' => $barang,
                'jumlah' => $jumlah,
                'total' => $total
            ]);
        
        return back();

    }
    public function cekSubtotalDetail(Request $request){
        

    }
    public function store(Request $request){
        $data = Session::all();
        

        $dataTransaksi = new Transaksi;
        $dataTransaksi->total_harga = array_sum($data['total']);
        $dataTransaksi->save();

        for($i = 0; $i <count($data['barang']); $i++){
            DB::table('transaksi_pembelian_barang')->insert([
                        'transaksi_pembelian_id' => $dataTransaksi->id, 
                        'master_barang_id' => $data['barang'][$i]->id,
                        'jumlah' => $data['jumlah'][$i],
                        'harga_satuan' => $data['barang'][$i]->harga_satuan,
                        ]);
        }
        $request->session()->reflash();
        return redirect()->route('transaksi.index');
    }
}
