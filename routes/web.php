<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\BarangController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Back End 
Route::group(['prefix' => '/', 'middleware' => ['auth']], function(){
    
    //transaksi
    Route::controller(TransaksiController::class)->group(function () {
        Route::get('transaksi/', 'index')->name('transaksi.index');
        Route::get('tambah-data-transaksi/', 'tambahTransaksi');
        Route::post('tambah-data-transaksi/cekSubtotal', 'cekSubtotal');
        Route::get('tambah-data-transaksi/store', 'store');

    });


    //barang
    Route::controller(BarangController::class)->group(function () {
        Route::get('barang/', 'index')->name('barang.index');
    });

   

});


