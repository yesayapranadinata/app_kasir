@extends('layouts.admin')

@section('tittle', 'Barang')

@section('tab_active_barang', 'active')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tabel Barang</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 5%; text-align: center">Id </th>
                        <th>Nama Barang</th>
                        <th>Harga Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataBarang as $item)
                        <tr>
                            <td style="text-align: center">{{ $item->id }}</td>
                            <td>{{ $item->nama_barang }}</td>
                            <td>Rp. {{ number_format($item->harga_satuan) }}</td>
                        </tr>
                    @endforeach
                    
                    
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection