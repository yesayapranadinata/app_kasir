@extends('layouts.admin')

@section('tittle', 'Transaksi')

@section('tab_active_transaksi', 'active')

@section('content')
<script src="https://code.jquery.com/jquery-3.6.2.min.js"
    integrity="sha256-2krYZKh//PcchRtd+H+VyyQoZ/e3EcrkxhM8ycwASPA=" crossorigin="anonymous"></script>
@php
    $data = Session::all();
    $barang = $data['barang'];
    $jumlah = $data['jumlah'];
    $total = $data['total'];


@endphp
<pre>
@php
        var_dump(Session::all());
@endphp
</pre>
@if(isset($data['barang']))
    <form action="{{ url('tambah-data-transaksi/cekSubtotal') }}" method="post">
        @csrf
        <div class="row">
            @for($i = 0; $i < count($data['barang']); $i++)
                <div class="col-md-5">
                    <label for="">Pilih Barang</label>
                    <select name="barang[]" class="form-control" id="exampleFormControlSelect1">
                        <option value="">{{ $barang[$i]->nama_barang }}</option>

                        @foreach($dataBarang as $item)
                            <option value="{{ $item }}">{{ $item->nama_barang }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="">Jumlah</label>
                    <input name="jumlah[]" class="form-control" type="number" min="0">
                </div>
                <div class="col-md-1">
                    <label for="">Tambah</label>

                    <a class="btn btn-primary" id="tambahTransaksiBarang" href="#"><i class="fa fa-plus"
                            aria-hidden="true"></i></a>
                </div>
            @endfor
        </div>


        <div id="tambahanBarang">

        </div>
        <br>
        <button type="submit" class="btn btn-success btn-lg btn-block">Cek Subtotal</button>
    </form>
    <br>
@else
    <form action="{{ url('tambah-data-transaksi/cekSubtotal') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-5">
                <label for="">Pilih Barang</label>
                <select name="barang[]" class="form-control" id="exampleFormControlSelect1">
                    @foreach($dataBarang as $item)
                        <option value="{{ $item }}">{{ $item->nama_barang }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label for="">Jumlah</label>
                <input name="jumlah[]" class="form-control" type="number" min="0">
            </div>
            <div class="col-md-1">
                <label for="">Tambah</label>

                <a class="btn btn-primary" id="tambahTransaksiBarang" href="#"><i class="fa fa-plus"
                        aria-hidden="true"></i></a>
            </div>
        </div>
        <div id="tambahanBarang">

        </div>
        <br>
        <button type="submit" class="btn btn-success btn-lg btn-block">Cek Subtotal</button>
    </form>
    <br>
@endif

<div class="row">
    <div class="col-md-5">

    </div>
    <div class="col-md-6">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Sub Total</th>
                    <th scope="col"><span id="total"></span></th>
                </tr>
            </thead>
        </table>
    </div>
    <pre>

    </pre>


    <script>
        $(document).on('click', "#tambahTransaksiBarang", function () {
            $("#tambahanBarang").append('<div class="row" style="padding-top: 2%">\
                <div class="col-md-5">\
                    <select name="barang[]" class="form-control" id="exampleFormControlSelect1">\
                        @foreach ($dataBarang as $item)\
                            <option value="{{ $item }}">{{ $item->nama_barang }}</option>\
                        @endforeach\
                    </select>\
                </div>\
                <div class="col-md-6">\
                    <input name="jumlah[]" class="form-control" type="number" min="0">\
                </div>\
                <div class="col-md-1">\
                    <a class="btn btn-danger removeTransaksi" id="removeTransaksi" href="#"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                </div>\
            </div>');
        });
        //hapus data 
        $(document).on('click', '.removeTransaksi', function () {
                $($(this).parents(".row")[0].remove());
            }
        );
    </script>
    @endsection