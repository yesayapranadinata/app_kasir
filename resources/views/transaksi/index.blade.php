@extends('layouts.admin')

@section('tittle', 'Transaksi')

@section('tab_active_transaksi', 'active')

@section('content')
<a href="{{ url('/tambah-data-transaksi') }}" class="btn btn-primary btn-icon-split">
    <span class="icon text-white-50">
        <i class="fas fa-info-circle"></i>
    </span>
    <span class="text">Tambah Transaksi</span>
</a>
<br><br>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tabel Transaksi</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Id Transaksi</th>
                        <th>Waktu Transaksi</th>
                        <th>Total Transaksi</th>
                        <th style="width: 5%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataTran as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>{{ $item->total_harga }}</td>
                        <td>
                            <a href="#" class="btn btn-info btn-icon-split" title="Info Transksi">
                                <span class="icon text-white-50">
                                    <i class="fas fa-info-circle"></i>
                                </span>
                            </a>
                        </td>

                    </tr>
                    @endforeach
                    
                    
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection