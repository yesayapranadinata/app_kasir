@extends('layouts.admin')

@section('tittle', 'Transaksi')

@section('tab_active_transaksi', 'active')

@section('content')
<script src="https://code.jquery.com/jquery-3.6.2.min.js"
    integrity="sha256-2krYZKh//PcchRtd+H+VyyQoZ/e3EcrkxhM8ycwASPA=" crossorigin="anonymous"></script>

@php
    $data = Session::all();
   
@endphp

@if(isset($data['barang']))
   @php
        $barang = $data['barang'];
        $jumlah = $data['jumlah'];
        $total = $data['total'];
   @endphp
    <form action="{{ url('tambah-data-transaksi/cekSubtotal') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-5">
                <label for="">Pilih Barang</label>
               
            </div>
            <div class="col-md-6">
                <label for="">Jumlah</label>
            </div>
            <div class="col-md-1">
                <label for="">Tambah</label>
                
            </div>
        </div>
        <div class="row" style="">
            
            @for($i = 0; $i < count($data['barang']); $i++)
                <div class="col-md-5" style="padding-top: 1%" >
                    <select name="barang[]" class="form-control" id="exampleFormControlSelect1">
                        <option value="{{ json_encode($barang[$i]) }}">{{ $barang[$i]->nama_barang }}</option>

                        @foreach($dataBarang as $item)
                            <option value="{{ $item }}">{{ $item->nama_barang }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6" style="padding-top: 1%">
                    <input name="jumlah[]" value="{{ $jumlah[$i] }}" class="form-control" type="number" min="0">
                </div>
                <div class="col-md-1" style="padding-top: 1%">
                    @if ($i == 0)
                        <a class="btn btn-primary" id="tambahTransaksiBarang" href="#"><i class="fa fa-plus"
                        aria-hidden="true"></i></a>
                    @else
                        <a class="btn btn-danger removeTransaksi" id="removeTransaksi" href="#"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    @endif
                   
                </div>
            @endfor
        </div>


        <div id="tambahanBarang">

        </div>
        <br>
        <button type="submit" class="btn btn-success btn-lg btn-block">Cek Subtotal</button>
    </form>
    <br>
@else
    <form action="{{ url('tambah-data-transaksi/cekSubtotal') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-5">
                <label for="">Pilih Barang</label>
                <select name="barang[]" class="form-control" id="exampleFormControlSelect1">
                    @foreach($dataBarang as $item)
                        <option value="{{ $item }}">{{ $item->nama_barang }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label for="">Jumlah</label>
                <input name="jumlah[]" class="form-control" type="number" min="0">
            </div>
            <div class="col-md-1">
                <label for="">Tambah</label>

                <a class="btn btn-primary" id="tambahTransaksiBarang" href="#"><i class="fa fa-plus"
                        aria-hidden="true"></i></a>
            </div>
        </div>
        <div id="tambahanBarang">

        </div>
        <br>
        <button type="submit" class="btn btn-success btn-lg btn-block">Cek Subtotal</button>
    </form>
    <br>
@endif


<div class="row">
   <div class="col md-12">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Jumlah</th>
                <th scope="col">Harga</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            @for($i = 0; $i < count($data['barang']); $i++)
            <tr>
                <td scope="row">{{ $barang[$i]->id }}</td>
                <td scope="row">{{ $barang[$i]->nama_barang }}</td>
                <td scope="row">{{ $jumlah[$i] }}</td>
                <td scope="row">Rp. {{ number_format($barang[$i]->harga_satuan) }}</td>
                <td scope="row">Rp. {{ number_format($total[$i]) }}</td>
            </tr>
            @endfor
            <tr>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row">Subtotal</th>
                <th scope="row">Rp. {{ number_format(array_sum($total)) }}</th>
            </tr>
        </tbody>
    </table>
   </div>
</div>
  
@if(isset($data['barang']))
<a href="{{ url('tambah-data-transaksi/store') }}" class="btn btn-success btn-lg btn-block">Simpan </a>
@endif

    <script>
        $(document).on('click', "#tambahTransaksiBarang", function () {
            $("#tambahanBarang").append('<div class="row" style="padding-top: 2%">\
                <div class="col-md-5">\
                    <select name="barang[]" class="form-control" id="exampleFormControlSelect1">\
                        @foreach ($dataBarang as $item)\
                            <option value="{{ $item }}">{{ $item->nama_barang }}</option>\
                        @endforeach\
                    </select>\
                </div>\
                <div class="col-md-6">\
                    <input name="jumlah[]" class="form-control" type="number" min="0">\
                </div>\
                <div class="col-md-1">\
                    <a class="btn btn-danger removeTransaksi" id="removeTransaksi" href="#"><i class="fa fa-minus" aria-hidden="true"></i></a>\
                </div>\
            </div>');
        });
        //hapus data 
        $(document).on('click', '.removeTransaksi', function () {
                $($(this).parents(".row")[0].remove());
            }
        );
    </script>
    @endsection